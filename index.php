<?php

require __DIR__ . '/vendor/autoload.php';

$title = 'parent-project-1';
$version = '1.0.0';

$child_model = new Child\models\Model();

$child_version = $child_model->getVersion();

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?= $title ?></title>
	</head>
	<body>
		<div class="parent">
			<h1><?= $title ?></h1>
			<h2><?= $version ?></h2>
		</div>
		<hr />
		<div class="child">
			<h1>Child</h1>
			<h2><?= $child_version ?></h2>
		</div>
	</body>
</html>
